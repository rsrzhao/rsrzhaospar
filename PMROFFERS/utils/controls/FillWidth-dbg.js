/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(["sap/ui/core/Control", "sap/ui/Device"], function(Control, Device){
	"use strict";
	function findSapUiParent(control){
		
		function traverse(item, maxDepth){
			var parent = item.parent();
			if(parent.data().hasOwnProperty("sapUi") || maxDepth >= 10){
				return parent;
			}
			return traverse(parent, maxDepth + 1);
		}
		
		return traverse(control, 0);
	}
	
	return Control.extend("retail.pmr.promotionaloffers.utils.controls.FillWidth", {
		renderer: {
			render : function(oRm, oControl){
				//it does not actually have a ui
				//so it just renders the control it has in it
				oRm.renderControl(oControl.getControl());
			}
		},
		metadata:{		
			aggregations : {
				control: {
                    type: "sap.ui.core.Control",
                    multiple: false
                }
			},
			defaultAggregation : "control"
		},
		onAfterRendering : function(){
			this.resizeControl(true);
			var that = this;
			
			var parent = findSapUiParent(that.getControl().$());
			var oldWidth = parent.width(); 
			this.timer = setInterval(function(){
				var newWidth = parent.width();
				if(newWidth !== oldWidth){			
					that.resizeControlHandler();
					oldWidth = newWidth;
				}
			}, 500);
			
		},
		onBeforeRendering :  function(){
			clearInterval(this.timer);
		},
		destroy: function(){
			this.getControl().destroy();
			clearInterval(this.timer);
		},
		resizeControlHandler : function(){
			this.resizeControl(false);
		},
		resizeControl : function(useMargin){
			var control = this.getControl();
			var $controlElement = control.$();
			var $parent = findSapUiParent($controlElement);
			var margins = useMargin ? parseInt($parent.css("padding-right"), 10) * 2 : 0;
			control.setWidth(($parent.width() - margins) + "px");
		}
	});

});