/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */

sap.ui.define([ "sap/m/ComboBox",
                "sap/m/ComboBoxRenderer",
                "sap/ui/core/Item",
                "sap/m/SelectList",
                "sap/m/SelectListRenderer"], function (ComboBox, ComboBoxRenderer, Item, SelectList, SelectListRenderer) {
	"use strict";
	var SECOND_LEVEL_IDENTIFIER = "SecondLevel";
	var PARENT_IDENTIFIER = "isParent";
	
	var SelectListHierarchy = SelectList.extend("retail.pmr.promotionaloffers.utils.controls.SelectListHierarchy",{
		renderer: SelectListRenderer.render,
		onAfterRendering: function() {
			var aItems = this.getItems() || [];
			for(var i = 0, iLen = aItems.length; i < iLen; i++) {
				if(aItems[i].data(SECOND_LEVEL_IDENTIFIER)) {
					aItems[i].$().css("text-indent", "1.5rem");
				} else if(aItems[i].data(PARENT_IDENTIFIER)) {
					aItems[i].$().removeClass("sapMSelectListItemBase sapMSelectListItemBaseInvisible sapMSelectListItemBaseDisabled sapMSelectListItemBaseHoverable");
					aItems[i].$().addClass("sapMSelectListItem");
				}
			}
			
		}
	});
	
	var HierarchicalComboBox = ComboBox.extend("retail.pmr.promotionaloffers.utils.controls.HierarchicalComboBox",{
		renderer: ComboBoxRenderer.render,
		init : function () {
	        // call the init function of the parent
			ComboBox.prototype.init.apply(this, arguments);
			 this._aParentItems = [];
			 this._config = {
					 binding: "TermStyles",
					 parentKey: "Group",
					 parentText: "GroupDescription"
			 };
		},
		metadata : {
			aggregations : {
				items: { type: "sap.ui.core.Item", multiple: true, singularName: "item", bindable: "bindable" }
			}
		}
	});
	
	HierarchicalComboBox.prototype.setConfig = function (conf) {
		if(!conf.binding || !conf.parentKey || !conf.parentText) {
			return;
		}
		 this._config = conf;
	};
	
	HierarchicalComboBox.prototype.addItem = function(oItem) {
		this.addAggregation("items", oItem);
		var oContext = oItem.getBindingContext(this._config.binding);
		if(!oContext) {
			return this;	
		}
		var addParent = function(oData) {
			var parentItem = new sap.ui.core.Item({key:oData[this._config.parentKey], text:oData[this._config.parentText]});
			parentItem.data(PARENT_IDENTIFIER, true);
			parentItem.setEnabled(false);
			this._aParentItems.push(parentItem);
			this.addItem(parentItem);
		}.bind(this);
		var oData = {};
		var sPath = oContext.getPath();
		oData = oContext.getModel().getProperty(sPath) || [];
		if(oData[this._config.parentText]) {
			oItem.data(SECOND_LEVEL_IDENTIFIER, oData[this._config.parentKey]);
			if (!this.findItem("key", oData[this._config.parentKey])) {
				addParent(oData);
			}
		}
		this.orderItems();
		return this;
	};

	HierarchicalComboBox.prototype.createList = function() {
		this._oList = new SelectListHierarchy({
			width: "100%"
		}).addStyleClass(this.getRenderer().CSS_CLASS + "List")
		.attachSelectionChange(this.onSelectionChange, this)
		.attachItemPress(this.onItemPress, this);
		return this._oList;
	};
		
	HierarchicalComboBox.prototype.getChildrensByParent = function(parent, aItems) {
		var items = [];
		for(var i = 0, iLen = aItems.length; i < iLen; i++) {
			if(!aItems[i].data(PARENT_IDENTIFIER) && aItems[i].data(SECOND_LEVEL_IDENTIFIER) === parent) {
				items.push(aItems[i]);
			}
		}
		return items;
	};
	
	HierarchicalComboBox.prototype.orderItems = function() {
		var aItems = this.getList().getItems();
		this.removeAllItems();
		var items = [];
		var itemsWithoutParent = [];
		var i, iLen;
		for(i = 0, iLen = aItems.length; i < iLen; i++) {
			if(aItems[i].data(PARENT_IDENTIFIER)) {
				items.push(aItems[i]);
				items = items.concat(this.getChildrensByParent(aItems[i].getKey(), aItems));
			}
			if(!aItems[i].data(PARENT_IDENTIFIER) && !aItems[i].data(SECOND_LEVEL_IDENTIFIER)) {
				itemsWithoutParent.push(aItems[i]);
			}
		}
		items = items.concat(itemsWithoutParent);
		for(i = 0, iLen = items.length; i < iLen; i++) {
			this.insertItem(items[i], i);
		}
	};

	HierarchicalComboBox.prototype.getItems = function() {
		var oList = this.getList();
		var items = [];
		if(oList) {
			var oListItems = oList.getItems();
			for(var i = 0, iLen = oListItems.length; i < iLen; i++) {
				if(!oListItems[i].data(PARENT_IDENTIFIER)) {
					items.push(oListItems[i]);
				}
			}
		}
		return items;
	};
	HierarchicalComboBox.prototype.oninput = function(oEvent) {
		var aItems = this._aParentItems;
		for(var i = 0, iLen = aItems.length; i < iLen; i++) {
			if(aItems[i].data(PARENT_IDENTIFIER)) {
				this._setItemVisibility(aItems[i], false);
			}
		}		
		ComboBox.prototype.oninput.apply(this, arguments);
	};
	
	return HierarchicalComboBox;
});